import { expect } from 'chai';
import Dek from '../src/dek';

describe('Stack', () => {
  let stack: Dek;
  beforeEach(() => {
    stack = new Dek();
  });

  it('should be constructable', () => {
    expect(stack).to.be.ok;
  });

  describe('when I add something', () => {
    const something = 'SOMETHING';
    beforeEach(() => {
      stack.push(something);
    });

    it('should give it back', () => {
      expect(stack.pop()).to.equal(something);
    });

    it('should not fail when I pop a lot', () => {
      expect(stack.pop()).to.equal(something);
      expect(stack.pop()).to.not.be.ok;
    });

    describe('and then another thing', () => {
      const anotherThing = 'ANOTHER THING';
      beforeEach(() => {
        stack.push(anotherThing);
      });

      it('should give all that back, in reverse', () => {
        expect(stack.pop()).to.equal(anotherThing);
        expect(stack.pop()).to.equal(something);
        expect(stack.pop()).to.not.be.ok;
      });
    });
  });
});
