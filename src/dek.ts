interface Stack {
  push: (value: any) => void;
  pop: () => any;
}

class Leaf {
  public next?: Leaf;
  private _value: any;
  constructor(value: any) {
    this._value = value;
  }

  get value() {
    return this._value;
  }
}

export default class Dek implements Stack {
  private _tip?: Leaf;
  push(value: any) {
    let newLeaf = new Leaf(value);
    newLeaf.next = this._tip;
    this._tip = newLeaf;
  }

  pop() {
    if (!this._tip) {
      return null;
    }

    let value = this._tip.value;
    this._tip = this._tip.next;
    return value;
  }
}
